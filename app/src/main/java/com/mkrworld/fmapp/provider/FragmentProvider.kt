package com.mkrworld.fmapp.provider

import androidx.fragment.app.Fragment
import com.mkrworld.fmapp.ui.fragments.LiveShowFragment
import com.mkrworld.fmapp.ui.fragments.VideoFragment
import com.mkrworld.fmapp.ui.fragments.VideoLibraryFragment


class FragmentProvider {

    /**
     * Interface to hold Tag
     */
    interface TAG {
        companion object {
            const val FRAGMENT_VIDEO: String = "FRAGMENT_VIDEO"
            const val FRAGMENT_VIDEO_LIBRARY: String = "FRAGMENT_VIDEO_LIBRARY"
            const val FRAGMENT_LIVE_SHOW: String = "FRAGMENT_LIVE_SHOW"
        }
    }

    companion object {

        /**
         * Method to find fragment by tag
         * @param tag
         */
        fun getFragment(tag: String): Fragment = when (tag) {
            TAG.FRAGMENT_VIDEO -> VideoFragment()
            TAG.FRAGMENT_VIDEO_LIBRARY -> VideoLibraryFragment()
            TAG.FRAGMENT_LIVE_SHOW -> LiveShowFragment()
            else -> Fragment()
        }
    }
}