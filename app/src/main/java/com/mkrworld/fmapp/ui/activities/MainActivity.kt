package com.mkrworld.fmapp.ui.activities

import android.Manifest
import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.lory.library.ui.ui.activity.MKRAppcompatActivity
import com.mkrworld.fmapp.R
import com.mkrworld.fmapp.provider.FragmentProvider
import com.mkrworld.fmapp.ui.Utils
import java.io.File


class MainActivity : MKRAppcompatActivity(), View.OnClickListener {
    override fun finishActivity() {

    }

    override fun getActivityLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getDefaultFragmentContainerId(): Int {
        return R.id.activity_main_container
    }

    override fun getRequiredPermissions(): Array<String> {
        return arrayOf(
            Manifest.permission.INTERNET,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
    }

    override fun init(intent: Intent?) {
        // initial fragment
        setCurrentFragment(FragmentProvider.TAG.FRAGMENT_VIDEO)

        // init bottom navigation and menu click
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_option_home -> setCurrentFragment(FragmentProvider.TAG.FRAGMENT_VIDEO)
                R.id.menu_option_video_library -> setCurrentFragment(FragmentProvider.TAG.FRAGMENT_VIDEO_LIBRARY)
                R.id.menu_option_live -> setCurrentFragment(FragmentProvider.TAG.FRAGMENT_LIVE_SHOW)
            }
            true
        }
    }

    private fun setCurrentFragment(tag: String) {
        val fragment = FragmentProvider.getFragment(tag)
        val bundle = Bundle()
        fragment.arguments = bundle
        onBaseActivityReplaceFragment(fragment, bundle, tag)
    }

    override fun onClick(v: View?) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkAndCallPermission()
    }
}