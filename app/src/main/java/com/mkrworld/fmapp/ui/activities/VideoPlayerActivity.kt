package com.mkrworld.fmapp.ui.activities

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.MediaController
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import com.mkrworld.fmapp.R

class VideoPlayerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_video_player)

        Log.d("newF", "VideoPlayerActivity")
        val mVideoView = findViewById<VideoView>(R.id.videoView)
        val url: Int? = intent?.getIntExtra("url", -1)

        val mediaController = MediaController(applicationContext)
        mediaController.setAnchorView(mVideoView)
        mVideoView.setVideoURI(Uri.parse("android.resource://" + applicationContext.getPackageName() + "/" + url))
        mVideoView.start()
        mVideoView.setOnPreparedListener { mp ->
            mp.start()
            val videoRatio = mp.videoWidth / mp.videoHeight.toFloat()
            val screenRatio = mVideoView.width / mVideoView.height.toFloat()
            val scale = videoRatio / screenRatio
            if (scale >= 1f) {
                mVideoView.scaleX = scale
            } else {
                mVideoView.scaleY = 1f / scale
            }
        }
        mVideoView.setOnCompletionListener { mp -> mp.start() }
    }
}