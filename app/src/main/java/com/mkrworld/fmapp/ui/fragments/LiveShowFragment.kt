package com.mkrworld.fmapp.ui.fragments

import android.app.ProgressDialog
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mkrworld.fmapp.R
import com.mkrworld.fmapp.dto.AssetsItem
import com.mkrworld.fmapp.dto.VideoItem
import com.mkrworld.fmapp.ui.Utils
import com.mkrworld.fmapp.ui.adapters.LiveShowAdapter
import java.util.ArrayList

class LiveShowFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_live_show, container, false)
        return inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    private fun initView(view: View) {
        val liveRecyclerView =
            view.findViewById<RecyclerView>(R.id.videoLibraryRecyclerView)
        liveRecyclerView.layoutManager = GridLayoutManager(activity!!, 1)
        val liveShowAdaptor = LiveShowAdapter(arrayListOf())
        liveRecyclerView.adapter = liveShowAdaptor

        val videoItems: MutableList<AssetsItem> = ArrayList()
        videoItems.add(AssetsItem("video1.mp4", R.raw.video1, R.drawable.live1))
        videoItems.add(AssetsItem("video2.mp4", R.raw.video2, R.drawable.live1))
        videoItems.add(AssetsItem("video3.mp4", R.raw.video3, R.drawable.live1))
        liveShowAdaptor.add(videoItems)
    }
}