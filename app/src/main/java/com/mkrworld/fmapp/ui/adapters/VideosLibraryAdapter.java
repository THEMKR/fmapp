package com.mkrworld.fmapp.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mkrworld.fmapp.R;
import com.mkrworld.fmapp.dto.AssetsItem;
import com.mkrworld.fmapp.dto.VideoItem;
import com.mkrworld.fmapp.ui.activities.VideoPlayerActivity;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class VideosLibraryAdapter extends RecyclerView.Adapter<VideosLibraryAdapter.VideoViewHolder>{
    private List<AssetsItem> mVideoItems;

    public VideosLibraryAdapter(List<AssetsItem> videoItems) {
        mVideoItems = videoItems;
    }

    @NonNull
    @Override
    public VideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VideoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_videos_library_container,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull VideoViewHolder holder, int position) {
        holder.setVideoData(mVideoItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mVideoItems.size();
    }

    public void add(@NotNull List<AssetsItem> videoItems) {
        mVideoItems = videoItems;
        notifyDataSetChanged();
    }

    static class VideoViewHolder extends RecyclerView.ViewHolder{
        ImageView mVideoImageView;
        TextView txtTitle,txtDesc;

        public VideoViewHolder(@NonNull View itemView) {
            super(itemView);
            mVideoImageView = itemView.findViewById(R.id.videoImageView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtTitle.setSelected(true);
            txtDesc = itemView.findViewById(R.id.txtDesc);
        }

        void setVideoData(AssetsItem videoItem){
            txtTitle.setText(videoItem.videoName);
            txtDesc.setText(videoItem.videoName);
            mVideoImageView.setImageResource(videoItem.iconId);
            mVideoImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, VideoPlayerActivity.class);
                    intent.putExtra("url", videoItem.videoId);
                    Log.d("newF", videoItem.videoName);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }
}
