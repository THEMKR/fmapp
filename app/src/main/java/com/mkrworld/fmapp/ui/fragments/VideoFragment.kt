package com.mkrworld.fmapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.mkrworld.fmapp.R
import com.mkrworld.fmapp.dto.AssetsItem
import com.mkrworld.fmapp.ui.adapters.VideosAdapter
import java.util.*

class VideoFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val inflate = inflater.inflate(R.layout.fragment_video, container, false)
        val videosViewPager: ViewPager2 = inflate.findViewById(R.id.viewPagerVideos)
        val videoItems: MutableList<AssetsItem> = ArrayList()
        videoItems.add(AssetsItem("video1.mp4", R.raw.video1, R.drawable.pic1))
        videoItems.add(AssetsItem("video2.mp4", R.raw.video2, R.drawable.pic2))
        videoItems.add(AssetsItem("video3.mp4", R.raw.video3, R.drawable.pic3))
        videoItems.add(AssetsItem("video1.mp4", R.raw.video1, R.drawable.pic1))
        videoItems.add(AssetsItem("video2.mp4", R.raw.video2, R.drawable.pic2))
        videoItems.add(AssetsItem("video3.mp4", R.raw.video3, R.drawable.pic3))
        videoItems.add(AssetsItem("video1.mp4", R.raw.video1, R.drawable.pic1))
        videoItems.add(AssetsItem("video2.mp4", R.raw.video2, R.drawable.pic2))
        videoItems.add(AssetsItem("video3.mp4", R.raw.video3, R.drawable.pic3))
        videoItems.add(AssetsItem("video1.mp4", R.raw.video1, R.drawable.pic1))
        videoItems.add(AssetsItem("video2.mp4", R.raw.video2, R.drawable.pic2))
        videoItems.add(AssetsItem("video3.mp4", R.raw.video3, R.drawable.pic3))
        videoItems.add(AssetsItem("video1.mp4", R.raw.video1, R.drawable.pic1))
        videoItems.add(AssetsItem("video2.mp4", R.raw.video2, R.drawable.pic2))
        videoItems.add(AssetsItem("video3.mp4", R.raw.video3, R.drawable.pic3))
        videoItems.add(AssetsItem("video1.mp4", R.raw.video1, R.drawable.pic1))
        videoItems.add(AssetsItem("video2.mp4", R.raw.video2, R.drawable.pic2))
        videoItems.add(AssetsItem("video3.mp4", R.raw.video3, R.drawable.pic3))

        videosViewPager.adapter = VideosAdapter(videoItems)
        return inflate
    }
}