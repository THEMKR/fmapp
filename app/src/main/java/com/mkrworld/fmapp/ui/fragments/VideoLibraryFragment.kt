package com.mkrworld.fmapp.ui.fragments

import android.app.ProgressDialog
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mkrworld.fmapp.R
import com.mkrworld.fmapp.dto.AssetsItem
import com.mkrworld.fmapp.dto.VideoItem
import com.mkrworld.fmapp.ui.Utils
import com.mkrworld.fmapp.ui.adapters.VideosLibraryAdapter
import java.util.ArrayList

class VideoLibraryFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_video_library, container, false)
        return inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    private fun initView(view: View) {
        val videoLibraryRecyclerView =
            view.findViewById<RecyclerView>(R.id.videoLibraryRecyclerView)
        videoLibraryRecyclerView.layoutManager = GridLayoutManager(activity!!, 1)
        val videosLibraryAdapter = VideosLibraryAdapter(arrayListOf())
        videoLibraryRecyclerView.adapter = videosLibraryAdapter

        val videoItems: MutableList<AssetsItem> = ArrayList()
        videoItems.add(AssetsItem("video1.mp4", R.raw.video1, R.drawable.pic1))
        videoItems.add(AssetsItem("video2.mp4", R.raw.video2, R.drawable.pic2))
        videoItems.add(AssetsItem("video3.mp4", R.raw.video3, R.drawable.pic3))

        videosLibraryAdapter.add(videoItems)
//        val progressDialog = ProgressDialog(activity)
//        progressDialog.setMessage("Loading...")
//        progressDialog.setCancelable(false)
//        Thread {
//            if (Utils.videoItems.isEmpty()) {
//                activity!!.runOnUiThread { progressDialog.show() }
//
//                Utils.getAllVideos(activity!!, 8).forEach {
//                    val item = VideoItem()
//                    item.videoURL = it[MediaStore.Video.Media.DATA]
//                    item.videoTitle = it[MediaStore.Video.Media.DISPLAY_NAME]
//                    item.videoDesc = it[MediaStore.Video.Media.DATE_MODIFIED]
//
//                    val options = BitmapFactory.Options()
//                    options.inSampleSize = 1
//                    item.thumbnail = ThumbnailUtils.createVideoThumbnail(
//                        item.videoURL,
//                        MediaStore.Video.Thumbnails.MINI_KIND
//                    )
//                    Utils.videoItems.add(item)
//                }
//            }
//
//            activity!!.runOnUiThread {
//                progressDialog.dismiss()
//                videosLibraryAdapter.add(Utils.videoItems)
//            }
//        }.start()
    }
}