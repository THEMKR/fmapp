package com.mkrworld.fmapp.ui

import android.app.Activity
import android.database.Cursor
import android.provider.MediaStore
import androidx.core.database.getLongOrNull
import com.mkrworld.fmapp.dto.VideoItem
import java.util.*
import kotlin.collections.HashMap

class Utils {
    companion object {
        val videoItems: MutableList<VideoItem> = ArrayList<VideoItem>()

        fun getAllVideos(activity: Activity, count: Int): ArrayList<HashMap<String, String?>> {
            val arrayList = arrayListOf<HashMap<String, String?>>()
            val arrayListUrls = arrayListOf<String>()
            val projection =
                arrayOf(
                    MediaStore.Video.VideoColumns.DATA,
                    MediaStore.Video.Media.DISPLAY_NAME,
                    MediaStore.Video.Media.DATE_MODIFIED
                )
            val cursor: Cursor? = activity.contentResolver
                .query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projection, null, null, null)
            try {
                var count = count;
                cursor!!.moveToFirst()
                do {
                    val url =
                        cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA))
                    var displayName =
                        cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME))
                    if (!arrayListUrls.contains(url)) {
                        arrayListUrls.add(url)
                        val videoItemHashMap: HashMap<String, String?> = HashMap()
                        videoItemHashMap.put(
                            MediaStore.Video.Media.DATA,
                            url
                        )
                        if (displayName.indexOf(".") > 0)
                            displayName = displayName.substring(0, displayName.lastIndexOf("."));
                        videoItemHashMap.put(
                            MediaStore.Video.Media.DISPLAY_NAME,
                            displayName
                        )
                        val date = cursor.getLongOrNull(
                            cursor.getColumnIndexOrThrow(
                                MediaStore.Video.Media.DATE_MODIFIED
                            )
                        )
                        val dateFormatted =
                            android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss a", date!!)
                                .toString()
                        videoItemHashMap.put(
                            MediaStore.Video.Media.DATE_MODIFIED,
                            dateFormatted
                        )
                        arrayList.add(videoItemHashMap)
                    }
                    --count;
                } while (cursor.moveToNext() && count > 0)
                cursor.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return arrayList
        }
    }
}