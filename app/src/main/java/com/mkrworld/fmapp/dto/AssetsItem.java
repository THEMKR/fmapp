package com.mkrworld.fmapp.dto;

public class AssetsItem {
    public String videoName;
    public int videoId;
    public int iconId;

    public AssetsItem(String videoName, int videoId, int iconId){
        this.videoName = videoName;
        this.videoId = videoId;
        this.iconId = iconId;
    }
}
