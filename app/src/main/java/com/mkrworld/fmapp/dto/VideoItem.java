package com.mkrworld.fmapp.dto;

import android.graphics.Bitmap;

public class VideoItem {
    public String videoURL,videoTitle,videoDesc;
    public Bitmap thumbnail;
}
